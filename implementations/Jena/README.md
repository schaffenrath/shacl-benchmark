#Jena Benchmark Java implementation

## Requirements 
The benchmark is designed to evaluate an already existing repository. This can easily be done by downloading [![Jena Download](https://jena.apache.org/download/index.cgi)] and using the provided `tdbloader.sh` in the `bin` folder. The directory, where the repository has been created has to be passed as the first argument of the benchmark.   

## Files in directory
This directory contains the main Java file `jena.java`. 

## Running the benchmark
To run the benchmark it is adviced to execute the `run-jena.sh` script, because it contains additional JVM flags that allocate more memory required for the benchmark. 
It is also possible to just execute `jena.jar` with Java and add the flag `-Xmx14G`