package jena;

import java.util.Iterator;

import org.apache.jena.atlas.logging.LogCtl;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.shacl.*;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.tdb.TDBFactory;

public class jena {
	static {
		LogCtl.setLog4j();
	}

	public static void main(String[] args) {
		String SHAPES = "";
		String DATASAVED = "";

		// parse input arguments
		if (args.length == 0 || args.length == 1 || args.length > 2) {
			System.out.println("Invalid arguments!\nUsage: java -jar jena.jar repository_dir shacl_file");
			System.exit(0);
		} else if (args.length == 2) {
			DATASAVED = args[0];
			SHAPES = args[1];
		}

		Graph shapesGraph = RDFDataMgr.loadGraph(SHAPES);
		DatasetGraph dataGraphSaved = TDBFactory.createDatasetGraph(DATASAVED);

		Graph data = null;
		for (Iterator<Node> iterator = dataGraphSaved.listGraphNodes(); iterator.hasNext();) {
			Node dsg = (Node) iterator.next();
			System.out.println(dsg);
			data = dataGraphSaved.getGraph(dsg);
			Shapes shapes = Shapes.parse(shapesGraph);

			ValidationReport report = ShaclValidator.get().validate(shapes, data);

			RDFDataMgr.write(System.out, report.getModel(), Lang.TTL);
		}

		shapesGraph.close();
		dataGraphSaved.close();
	}
}
