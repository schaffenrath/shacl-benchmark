#!/usr/bin/env bash
wget -nc http://mirror.klaus-uwe.me/apache/jena/binaries/apache-jena-3.13.1.tar.gz
wget -nc https://www.apache.org/dist/jena/binaries/apache-jena-3.13.1.tar.gz.sha512
sha512sum -c apache-jena-3.13.1.tar.gz.sha512 &&
tar -xf apache-jena-3.13.1.tar.gz
