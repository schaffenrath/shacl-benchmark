#!/bin/bash
if [ "$#" -eq 2 ]; then
    java -Xmx14G -jar jena.jar $1 $2 
    exit 0
fi
if [ "$#" -eq 3 ]; then
    java -Xmx14G -jar jena.jar $1 $2 > $3
    exit 0
fi 

echo "Invalid arguments!"
echo "Usage: run-jena.sh repository_dir shacl_file [output_file]"
exit 1

java -Xmx14G -jar jena.jar ./shacl_constraints/shacl_constraints.ttl ../jena5gbtdb1/   #update path for jena directory if needed
