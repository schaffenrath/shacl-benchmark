import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.vocabulary.RDF4J;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.SailRepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.eclipse.rdf4j.sail.shacl.ShaclSail;
import org.eclipse.rdf4j.sail.shacl.ShaclSailValidationException;

public class ShaclConnector {
	public static void main(String[] args) {
		// indices with which the database is created or the existing database has been
		// indexed
		final String indexes = "spoc,posc";
		// default storage place of database
		String databaseDirectory = "";
		String shaclFile = "";

		// parse input arguments
		if (args.length == 0 || args.length == 1 || args.length > 2) {
			System.out.println("Invalid arguments!\nUsage: java -jar RDF4J-Benchmark.jar repository_dir shacl_file");
			System.exit(0);
		} else if (args.length == 2) {
			databaseDirectory = args[0];
			shaclFile = args[1];
		}

		// open NativeStore with indexing at argument dir
		ShaclSail sail = new ShaclSail(new NativeStore(new File(databaseDirectory), indexes));
		SailRepository sailRepository = new SailRepository(sail);

		sailRepository.init();

		try (SailRepositoryConnection con = sailRepository.getConnection()) {
			System.out.println("Begin to copy constraints");
			con.begin();
			Reader shaclReader = null;

			try {
				shaclReader = new FileReader(shaclFile);
			} catch (IOException e) {
				System.out.println("Failed to read Shacl Shape File.");
				e.printStackTrace();
			}

			con.add(shaclReader, "", RDFFormat.TURTLE, RDF4J.SHACL_SHAPE_GRAPH);
			System.out.println("Finished copying constraints");

			long startTime = 0;
			try {
				System.out.println("Begin validation");
				startTime = System.currentTimeMillis();
				con.commit();
			} catch (RepositoryException ex) {
				Throwable cause = ex.getCause();
				if (cause instanceof ShaclSailValidationException) {
					Model validationReportModel = ((ShaclSailValidationException) cause).validationReportAsModel();
					Rio.write(validationReportModel, System.out, RDFFormat.TURTLE);
				}
			}
			long timeElapsed = System.currentTimeMillis() - startTime;
			con.close();
			System.out.println("Finished validation\nValdiation took " + timeElapsed + " milliseconds");
		} catch (RDF4JException e) {
			System.out.println("Failed to connect to repo");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Failed to add shapes.");
			e.printStackTrace();
		}
	}
}
