#!/bin/bash
if [ "$#" -eq 2 ]; then
    java -Xms4G -Xmx18G -jar RDF4J-Benchmark.jar $1 $2 
    exit 0
fi
if [ "$#" -eq 3 ]; then
    java -Xms4G -Xmx18G -jar RDF4J-Benchmark.jar $1 $2 > $3
    exit 0
fi 

echo "Invalid arguments!"
echo "Usage: run-rdf4j.sh repository_dir shacl_file [output_file]"
exit 1