#RDF4J Benchmark Java implementation

## Requirements 
The benchmark is designed to evaluate an already existing repository, which is indexed with `spoc` and `posc`. This can easily be done by downloading [![RDF4J Download](https://rdf4j.org/download/)] and using the provided `console.sh` in the `bin` folder. The directory, where the repository has been created has to be passed as the first argument of the benchmark.   

## Files in directory
This directory contains the main Java file `ShaclConnector.java` as well as the project directory `SHACL_RDF4J` which contains Eclipse configurations and a Maven `pom.xml` file. 

## Running the benchmark
To run the benchmark it is adviced to execute the `run-rdf4j.sh` script, because it contains additional JVM flags that allocate more memory required for the benchmark. 
It is also possible to just execute `RDF4J-Benchmark.jar` with Java and add the flag `-Xmx18G`